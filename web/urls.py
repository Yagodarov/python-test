from django.urls import path

from . import views

urlpatterns = [
    # ex: /web/
    path('', views.index, name='index'),
    # ex: /web/5/
    path('deal/', views.deal, name='client')
]
