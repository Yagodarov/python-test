from io import BytesIO, TextIOWrapper, StringIO
import csv

from web.models import Deal


def handle_clients_file(f, *args, **kwargs):
    Deal.objects.all().delete()
    csvf = StringIO(f.read().decode())
    reader = csv.reader(csvf, delimiter=',')
    next(reader)
    for row in reader:
        deal = Deal(customer=row[0], item=row[1], total=row[2], quantity=row[3], date=row[4])
        deal.save()
