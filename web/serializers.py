import sqlite3

def get_deals():
    conn = sqlite3.connect('db.sqlite3')
    cursor = conn.execute("""
    select web_deal.customer, (
        SELECT group_concat(DISTINCT item)
        ) as gems, sum(web_deal.total) as spent_money
    from web_deal
    where web_deal.item in (
        select item
        from
            (select web_deal.customer as customer,
                    web_deal.item
             from web_deal
             where customer IN (
                 select web_deal.customer from web_deal
                 group by web_deal.customer
                 order by count(web_deal.total) DESC
                 limit 5
             )
             group by web_deal.customer, web_deal.item
             order by sum(web_deal.total) DESC)
        group by item
        having count(item) > 1
    )
    group by web_deal.customer
    order by count(web_deal.total) DESC
    limit 5
""")
    results = cursor.fetchall()
    fetched_result = []
    for result in results:
        fetched_result.append({
            'username': result[0],
            'spent_money': result[1],
            'gems': result[2],
        })
    return fetched_result
