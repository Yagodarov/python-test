from django.db import models

class Deal(models.Model):
    customer = models.CharField(max_length=16)
    item = models.CharField(max_length=16)
    total = models.PositiveIntegerField()
    quantity = models.PositiveIntegerField()
    date = models.DateTimeField('deal_date')
    def __str__(self):
        return self.customer
