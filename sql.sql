select item
from
    (select web_deal.customer as customer, sum(web_deal.total) as total,
            web_deal.item
     from web_deal
     where customer IN (
         select web_deal.customer from web_deal
         group by web_deal.customer
         order by count(web_deal.total) DESC
         limit 5
    )
group by web_deal.customer, web_deal.item
order by sum(web_deal.total) DESC)
group by item
having count(item) > 1
