import json

# Create your views here.
from django.http import HttpResponse
from django.views.decorators.http import require_http_methods

from web.file_parsers import handle_clients_file
from web.forms import UploadFileForm
from web.serializers import get_deals

def index(request):
    status = 'error'
    desc = 'Not implemented'
    response_data = {
        'status': status,
        'desc': desc
    }
    return HttpResponse(json.dumps(response_data), content_type="application/json")

@require_http_methods(["GET", "POST"])
def deal(request):
    status = 'error'
    desc = 'Unknown error'
    try:
        status = 'OK'
        if request.method == "POST":
            form = UploadFileForm(request.POST, request.FILES)
            if form.is_valid():
                file = request.FILES['deals']
                handle_clients_file(file, request=request)
                desc = 'Upload success'
            else:
                status = 'error'
                desc = form.errors
            response_data = {
                'status': status,
                'desc': desc,
            }
            return HttpResponse(json.dumps(response_data), content_type="application/json")
        if request.method == "GET":
            deals = get_deals()
            response_data = {
                'status': status,
                'deals': deals
            }
            return HttpResponse(json.dumps(response_data), content_type="application/json")
    except Exception as exc:
        status = 'error'
        desc = exc.args
    response_data = {
        'status': status,
        'desc': desc
    }

    return HttpResponse(json.dumps(response_data), content_type="application/json")
