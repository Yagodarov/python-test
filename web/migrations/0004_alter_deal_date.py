# Generated by Django 3.2.3 on 2021-07-07 14:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0003_rename_deals_deal'),
    ]

    operations = [
        migrations.AlterField(
            model_name='deal',
            name='date',
            field=models.DateTimeField(verbose_name='deal_date'),
        ),
    ]
